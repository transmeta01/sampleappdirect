# README #

sample app for AppDirect dev API

### What is this repository for? ###

* Quick summary
* Version : 0.01.SNAPSHOT

### How do I get set up? ###

* To simulate responses of subscription events that you would get from the appDirect server
    * copy the json response you would get into the testdata dir
    * cd into the testdata and 
    * launch a python serve the json resource : `python -m SimpleHTTPServer 8081` 
    * note: run this server on port 8081, spring-boot on 8080 by default (you may change either one...makes sure they run on different ports)
    
* Security
    * a basic security mechanism is provided by the spring-security module 
    * it generate a security token to be used by default and printed on the console (log)
    * the key must be included in every call : example when testing with `curl`
    * `curl -X GET user:$key@localholhost:8080/v1/subscription?url=http:www.someur`
* Configuration (see application.properties)
* Dependencies (modify the build.gradle file to add or remove deps)
* Database configuration (db deps are in the application.properties file)
* How to run the local server
    * `gradle bootRun`
* How to debug
    * the build process is configured to bind port 9000 for debug, just configure your IDE to bind to remote server on port 9000

### Contribution guidelines ###

* Code review
* Other guidelines

### Who do I talk to? ###

* 