package com.example.appdirect.controller;

import com.example.appdirect.domain.response.ErrorResponse;
import com.example.appdirect.domain.Subscription;
import com.example.appdirect.domain.response.Response;
import com.example.appdirect.domain.response.SuccessResponse;
import com.example.appdirect.repository.SubscriptionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Random;
import java.util.UUID;

/**
 * Created by richard on 2017-01-14.
 *
 */

@RestController
@RequestMapping(value = "/v1")
public class SubscriptionController {
    private String accountIdentifier = UUID.randomUUID().toString();
    private final Random randomizer = new Random();
    private Logger logger = LoggerFactory.getLogger(Subscription.class);

    @Autowired
    SubscriptionRepository repo;

    private enum Type {
        SUBSCRIPTION_ORDER {
            @Override
            public String toString() {
                return "SUBSCRIPTION_ORDER";
            }
        },
      SUBSCRIPTION_CHANGE {
            @Override
            public String toString() {
                return "SUBSCRIPTION_CHANGE";
            }
        },
        SUBSCRIPTION_CREATE {
            @Override
            public String toString() {
                return "SUBSCRIPTION_CREATE";
            }
        },
        SUBSCRIPTION_CANCEL {
            @Override
            public String toString() {
                return "SUBSCRIPTION_CREATE";
            }
        };

        public static Type find(String name) {
            for(Type type: Type.values()) {
                if(type.name().equals(name)) return type;
            }

            return null;
        }
    };

    /**
     * Exceptions omitted for brevity :
     *      url may be malformed
     *      service may return 50*
     *      service provide a bogus URL resulting in 404
     *
     * @param eventURL
     * @return
     */
    @RequestMapping(value = "/subscription", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response processSubscription(@RequestParam("url") String eventURL) {
        RestTemplate restTemplate = new RestTemplate();
        Subscription detail = restTemplate.getForObject(eventURL, Subscription.class);
        Type type = Type.find(detail.getType());

        switch (type) {
            case SUBSCRIPTION_ORDER:
                logger.info("processing subscription order");
                return getResponse(detail);
            case SUBSCRIPTION_CREATE:

                logger.info("processing subscription create");
                return getResponse(detail);
            case SUBSCRIPTION_CANCEL:
                logger.info("processing subscription cancel");
                return getResponse(detail);
            case SUBSCRIPTION_CHANGE:
                logger.info("processing subscription change");
                return getResponse(detail);
            default: break; // ignore
        }

        return null; // or some other generic error code/msg
    }

    // simulate validation
    private Boolean validate(Subscription subscription) {
        return randomizer.nextBoolean();
    }

    /**
     * fake outcome of processing
     * @param Subscription subscription
     * @return Response
     */
    private Response getResponse(Subscription subscription) {

        String message = validate(subscription) ? "" : "Some kind of error happened";
        if(message.equals("")) {
            return new SuccessResponse(accountIdentifier);
        }  else {
            return new ErrorResponse(accountIdentifier, "", message);
        }
    }
}
