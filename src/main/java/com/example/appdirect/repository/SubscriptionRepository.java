package com.example.appdirect.repository;

import com.example.appdirect.domain.Creator;
import com.example.appdirect.domain.Subscription;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by richard on 2017-01-18.
 */
public interface SubscriptionRepository extends JpaRepository<Subscription, Long> {
    public Subscription getSubscriptionCreator(Creator creator);
}
