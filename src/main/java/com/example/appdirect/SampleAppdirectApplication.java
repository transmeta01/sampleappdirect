package com.example.appdirect;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleAppdirectApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampleAppdirectApplication.class, args);
	}
}
