package com.example.appdirect.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * Created by richard on 2017-01-14.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Address extends BaseEntity {

    private String city;
    private String country;
    private String firstName;
    private String lastName;
}
