package com.example.appdirect.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by richard on 2017-01-14.
 */
@Data
@EqualsAndHashCode(callSuper=true)
public class Creator extends BaseEntity {

    private Address address;
    private String email;
    private String firstName;
    private String lastName;
    private String locale;
    private String openId;
    private String uuid;
}
