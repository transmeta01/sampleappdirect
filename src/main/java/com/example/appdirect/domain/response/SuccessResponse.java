package com.example.appdirect.domain.response;

/**
 * Created by richard on 2017-01-14.
 */
public class SuccessResponse implements Response {
    private Boolean success = Boolean.TRUE;
    private String accountIdentifier;

    public SuccessResponse(String accountIdentifier) {
        setSuccess(Boolean.TRUE);
        setAccountIdentifier(accountIdentifier);
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public void setAccountIdentifier(String accountIdentifier) {
        this.accountIdentifier = accountIdentifier;
    }
}
