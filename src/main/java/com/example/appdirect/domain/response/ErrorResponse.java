package com.example.appdirect.domain.response;

/**
 * Created by richard on 2017-01-14.
 *
 * ErrorResponse to a notification request
 */

public class ErrorResponse extends SuccessResponse {
    private Boolean success = Boolean.FALSE;
    private String errorCode;
    private String message;

    public ErrorResponse(String accountIdentifier, String errorCode, String message) {
        super(accountIdentifier);
        setErrorCode(errorCode);
        setMessage(message);
    }

    public Boolean getSuccess() {
        return success;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ErrorResponse)) return false;

        ErrorResponse that = (ErrorResponse) o;

        if (getSuccess() != null ? !getSuccess().equals(that.getSuccess()) : that.getSuccess() != null) return false;
        if (getErrorCode() != null ? !getErrorCode().equals(that.getErrorCode()) : that.getErrorCode() != null)
            return false;
        return getMessage() != null ? getMessage().equals(that.getMessage()) : that.getMessage() == null;
    }

    @Override
    public int hashCode() {
        int result = getSuccess() != null ? getSuccess().hashCode() : 0;
        result = 31 * result + (getErrorCode() != null ? getErrorCode().hashCode() : 0);
        result = 31 * result + (getMessage() != null ? getMessage().hashCode() : 0);
        return result;
    }
}
