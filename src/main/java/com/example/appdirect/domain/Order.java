package com.example.appdirect.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;

/**
 * Created by richard on 2017-01-14.
 */
@Data
@EqualsAndHashCode(callSuper=true)
public class Order extends BaseEntity {
    private String editionCode;
    private String pricingDuration;
    private ArrayList<Item> items;
}
