package com.example.appdirect.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by richard on 2017-01-14.
 */
@Data
@EqualsAndHashCode(callSuper=true)
public class Account extends BaseEntity {
    private String accountIdentifier;
    private String status;
}
